#include <stdio.h>

int main(){
  //int a[5] = {-2,-1,2,3,4};
  //unsigned int found = gcd(50,15);
  //printf("found %u\n",found);
  return 0;
}

//********************** STACK ADT LIST **********************
typdef struct node *node_ptr;
struct node{
  element_type element;
  node_ptr next;
};
typedef node_ptr STACK;

int is_empty(STACK S){
  return (S->next == NULL);
}

STACK create_stack(void){
  STACK S;
  S = (STACK)malloc(sizeof(struct node));
  if(S==NULL) fatal_error("Out of space!!!");
  return S;
}

void make_null(STACK S){
  if(S!=NULL) S->next =NULL;
  else error("Must use create_stack first");
}

void push(element_type x, STACK S){
  node_ptr tmp_cell;
  tmp_cell = (node_ptr)malloc(sizeof(struct node));
  if(tmp_cell == NULL) fatal_error("Out of space!!!")
  else {
    tmp_cell->element = x;
    tmp_cell->next = S->next;
    S->next = tmp_cell;
  }
}

element_type top(STACK S){
  if(is_empty(S)) error("Empty stack")
  else return S->next->element;
}

void pop(){
  node_ptr first_cell;
  if(is_empty(S)) error("Empty stack")
  else{
    first_cell = S->next;
    S->next = S->next->next;
    free(first_cell);
  }
}
