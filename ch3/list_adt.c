#include <stdio.h>

int main(){
  //int a[5] = {-2,-1,2,3,4};
  //unsigned int found = gcd(50,15);
  //printf("found %u\n",found);
  return 0;
}
typedef int element_type;
typedef struct node *node_ptr;
struct node{
  element_type element;
  node_ptr next;
};

typedef node_ptr LIST;
typedef node_ptr position;

int is_empty(LIST L){
  return (L->next == NULL);
}

int is_last(position p, LIST L){
  return (p->next == NULL);
}

position find(element_type x, LIST L){
  position p;
  p = L->next;
  while((p!=NULL) && (p->element !=x))
    p = p->next;
  return p;
}

void delete(element_type x, LIST L){
  position p, tmp_cell;
  p = find_previous(x,L);
  if(p->next !=NULL){
    tmp_cell = p->next;
    p->next = tmp_cell->next;
    free(tmp_cell);
  }
}

position find_previous(element_type x, LIST L){
  position p;
  p = L;
  while((p!=NULL) && (p->element !=x))
    p = p->next;
  return p;
}

void insert(element_type x, LIST L, position p){
  position tmp_cell;
  tmp_cell = (position)malloc(sizeof(struct node));
  if(tmp_cell==NULL)
    fatal_error("Out of space!!!");
  else{
    tmp_cell->element = x;
    tmp_cell->next = p->next;
    p->next = tmp_cell;
  }
}

void delete_list(LIST L){
  position p, tmp;
  p = L->next;
  L->next = NULL:
  while(p!=NULL){
    tmp = p->next;
    free(p)
    p = tmp;
  }
}
