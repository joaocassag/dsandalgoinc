#include <stdio.h>

int max_subsequence_sum(int a[], unsigned int n);
int max_sub_sum(int a[], int left, int right, int* counter);
int max3(int mls, int mrs,int mlbs_mrbs);
void iterate_side_left(int* max_side_border_sum, int* side_border_sum, int center,
   int left,int a[], int* counter);
void iterate_side_right(int* max_side_border_sum, int* side_border_sum, int center,
   int right,int a[], int* counter);

int counter=0;
int l_counter = -1;
int r_counter = 1;

int main(){
  //int a[11] = {-1,2,4,-2,3,2,3,4,50,2,3};
  int a[5] = {-1,2,4,-2,3};
  int max = max_subsequence_sum(a,5);
  printf("max is %d\n",max);
  printf("counter is %d\n",counter);//total=12

  return 0;
}

int max_subsequence_sum(int a[], unsigned int n){

    return max_sub_sum(a,0,n-1, &counter);
}

int max_sub_sum(int a[], int left, int right, int* counter){
  int max_left_sum, max_right_sum;
  int max_left_border_sum, max_right_border_sum;
  int left_border_sum, right_border_sum;
  int center, i;

  if(left == right){
    if(a[left]>0){
      return a[left];
    }else {return 0;}
  }

  center = (left+right)/2;
  max_left_sum = max_sub_sum(a,left,center,counter);
  max_right_sum = max_sub_sum(a,center+1,right,counter);

  iterate_side_left(&max_left_border_sum,&left_border_sum,center,left,a,counter);

  iterate_side_right(&max_right_border_sum,&right_border_sum,center,right,a,counter);

  printf("counter is %d\n", *counter);
  //printf("r_counter is %d\n", r_counter);
  return max3(max_left_sum,max_right_sum,max_left_border_sum+max_right_border_sum);
}

int max3(int mls, int mrs,int mlbs_mrbs){
  if(mls>=mrs){
    if(mls>=mlbs_mrbs)return mls;
    else return mlbs_mrbs;

  }else{
    if(mrs>=mlbs_mrbs)return mrs;
    else return mlbs_mrbs;
  }
}

void iterate_side_left(int* max_side_border_sum, int* side_border_sum, int center,
   int left, int a[], int* counter){
  (*max_side_border_sum)=0;
  (*side_border_sum)=0;
  for(int i=center;i>=left;i--){
    (*counter)++;

    (*side_border_sum)+=a[i];
    if((*side_border_sum) > (*max_side_border_sum)){
      (*max_side_border_sum) = (*side_border_sum);
    }
  }
}

void iterate_side_right(int* max_side_border_sum, int* side_border_sum, int center,
   int right,int a[], int* counter){
  (*max_side_border_sum)=0;
  (*side_border_sum)=0;
  for(int i=center+1;i<=right;i++){
    (*counter)++;

    (*side_border_sum)+=a[i];
    if((*side_border_sum) > (*max_side_border_sum)){
      (*max_side_border_sum) = (*side_border_sum);
    }
  }
}
