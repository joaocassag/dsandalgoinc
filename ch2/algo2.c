#include <stdio.h>

int max_subsequence_sum(int a[], unsigned int n);

int counter=0;

int main(){
  //int a[11] = {-1,2,4,-2,3,2,3,4,50,2,3};
  int a[5] = {-1,2,4,-2,3};
  int max = max_subsequence_sum(a,5);
  printf("max is %d\n",max);
  printf("counter is %d\n",counter);//total=15

  return 0;
}

int max_subsequence_sum(int a[], unsigned int n){
  //printf("entered max_subsequence_sum(..)");
  int this_sum, max_sum, best_i, best_j, i, j, k;
  max_sum = 0; best_i = best_j = -1;
  for(int i=0;i<n;i++){
      this_sum =0;
      for(j=i;j<n;j++){
        this_sum += a[j];
        counter++;

        if(this_sum> max_sum){
          max_sum = this_sum;
          best_i = i;
          best_j = j;
        }
      }
    }
    //printf("leaving max_subsequence_sum(..)");
    return(max_sum);
}
