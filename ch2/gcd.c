#include <stdio.h>

unsigned int gcd(unsigned int m, unsigned int n);

int main(){

  //int a[5] = {-2,-1,2,3,4};
  unsigned int found = gcd(50,15);
  printf("found %u\n",found);

  return 0;
}

unsigned int gcd(unsigned int m, unsigned int n){
  unsigned int rem;
  while(n>0){
    rem = m%n;
    m = n;
    n = rem;
  }
  return m;
}
