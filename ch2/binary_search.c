#include <stdio.h>

int binary_search(int a[], int x, unsigned int n);

int main(){

  int a[5] = {-2,-1,2,3,4};
  int found = binary_search(a,0,5);
  printf("found %d\n",found);

  return 0;
}

int binary_search(int a[], int x, unsigned int n){
  int low, mid, high;
  low = 0; high = n-1;
  while(low<=high){
    mid = (low+high)/2;
    if(a[mid]<x) low = mid+1;
    else if(a[mid]>x) high = mid-1;
    else return mid;
  }
  //not found
  return -1000;//asuming -1000 is not in the array
}
