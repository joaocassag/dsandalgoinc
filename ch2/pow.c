#include <stdio.h>

int pow_ex(int x, unsigned int n);

int main(){

  //int a[5] = {-2,-1,2,3,4};
  int found = pow_ex(2,8);
  printf("found %u\n",found);

  return 0;
}

int pow_ex(int x, unsigned int n){
  if(n==0) return 1;
  if(n==1) return x;
  if((n%2)==0) return pow_ex(x*x,n/2);
  else return (pow_ex(x*x,n/2)*x);
}
