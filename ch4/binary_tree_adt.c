#include <stdio.h>

int main(){
  //int a[5] = {-2,-1,2,3,4};
  //unsigned int found = gcd(50,15);
  //printf("found %u\n",found);
  return 0;
}

//********************** BINARY TREE ADT **********************
struct tree_node{
  element_type elemet;
  tree_ptr left;
  tree_ptr right;
};
typedef struct tree_node *tree_ptr;
typedef tree_ptr SEARCH_TREE;

SEARCH_TREE make_null(void){
  return NULL;
}

tree_ptr find(element_type x, SEARCH_TREE T){
  if(T==NULL) return NULL;
  if(x<T->element)
    return find(x,T->left);
  else if(x>T->element)
    return find(x,T->right);
  else
    return T;
}

tree_ptr find_min(SEARCH_TREE T){
  if(T==NULL)
    return NULL;
  else if (T->left == NULL)
    return T;
  else
    return find(T->left);
}

tree_ptr find_max(SEARCH_TREE T){
  if(T!=NULL)
    while(T->right !=NULL)
      T = T->right;
  return T;
}

tree_ptr insert(element_type x, SEARCH_TREE T){
  if(T==NULL){
    T = (SEARCH_TREE) malloc (sizeof(struct tree_node));
    if(T==NULL) fatal_error("Out of space");
    else{
      T->elemet = x;
      T->left = T->right = NULL;
    }
  }
  else
    if(x<T->elemet)
      T->left = insert(x,T->left);
  else
    if(x>T->elemet)
      T->right = insert(x,T->right);
  return T;
}

tree_ptr delete(element_type x, SEARCH_TREE T){
  tree_ptr tmp_cell, child;
  if(T==NULL) error("Element not found")
  else
    if(x<T->elemet)
      T->left = delete(x,T->left);
  else
    if (x>T->elemet)
      T->right = delete(x, T->right);
  else
    if(T->left && T->right){
      tmp_cell = find_min(T->right);
      T->element = tmp_cell->element;
      T->right = delete(T->element,T->right);
    }else{
      tmp_cell = T;
      if(T->left == NULL) child = T->right;
      if(T->right == NULL) child = T->left;
      free(tmp_cell);
      return child;
    }
    return T;
}
