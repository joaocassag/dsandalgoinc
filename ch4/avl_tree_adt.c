#include <stdio.h>

int main(){
  //int a[5] = {-2,-1,2,3,4};
  //unsigned int found = gcd(50,15);
  //printf("found %u\n",found);
  return 0;
}

//********************** AVL TREE ADT **********************

typedef struct avl_node *avl_ptr;
struct avl_node{
  element_type element;
  avl_ptr left;
  avl_ptr right;
  int height;
};

typedef avl_ptr SEARCH_TREE;

int height(avl_ptr p){
  if(p==NULL) return -1;
  else return p->height;
}

SEARCH_TREE insert(element_type x, SEARCH_TREE T){
  return insert1(x, T, NULL);
}

SEARCH_TREE insert1(element_type x, SEARCH_TREE T, avl_ptr parent){
  avl_ptr rotated_tree;
  if(T==NULL){
    T = (SEARCH_TREE) malloc(sizeof(struct avl_node));
    if(T==NULL) fatal_error("Out of space!!!");
    else{
      T->element = x;
      T->height = 0;
      T->left = T->right = NULL;
    }
  }
  else{
    if(x<T->element){
      T->left = insert1(x,T->left, T);
      if( ( (height(T->left) - height(T->right)) == 2) ){
        if(x<T->left->element)
          rotated_tree = s_rotate_left(T);
        else
          rotated_tree = d_rotate_left(T);
        if(parent->left == T)
          parent->left = rotated_tree;
        else
          parent->right = rotated_tree;
      }
      else
        T->height = max( height(T->left),height(T->right))+1
    }
    else
      ; /*Symmetric Case for the right subtree; else x is in the tree already and we'll do nothing*/
  }
  return T;
}

avl_ptr s_rotate_left(avl_ptr k2){
  avl_ptr k1;
  k1 = k2->left;
  k2->left = k1->right;
  k1->right = k2;
  k2->height = max( height(k2->left),height(k2->right))+1;
  k1->height = max( height(k1->left),k2->height)+1;
  return k1;
}

avl_ptr d_rotate_left(avl_ptr k3){
  k3->left = s_rotate_right(k3->left);
  return s_rotate_left(k3);
}
