#include <stdio.h>

int main(){
  //int a[5] = {-2,-1,2,3,4};
  //unsigned int found = gcd(50,15);
  //printf("found %u\n",found);
  return 0;
}

//********************** SPLAY TREE ADT **********************
typedef struct splay_node *splay_ptr;
struct splay_node{
  element_type element;
  splay_ptr left;
  splay_ptr right;
  splay_ptr parent;
}
typedef splay_ptr SEARCH_TREE;

void splay(splay_ptr current){
  splay_ptr father;
  father = current->parent;
  while(father!=NULL){
    if(father->parent == NULL)
      single_rotate(current);
    else
      double_rotate(current);
    father = current->parent;
  }
}

void single single_rotate(splay_ptr x){
  if(x->parent->left == x)
    zig_left(x);
  else
    zig_right(x);
}

void zig_left(splay_ptr x){
  splay_ptr p, B;
  p = x->parent;
  B = x->right;
  x->right = p;
  x->parent = NULL;
  if(B!=NULL)
    B->parent = p;
  p->left = B;
  p->parent = x;
}

void zig_zig_left(splay_ptr x){
  splay_ptr p,g,B,C,ggp;
  p = x->parent;
  g = p->parent;
  B = x->right;
  C = p->right;
  ggp = g->parent;
  x->right = p;
  p->parent = x;
  p->right = g;
  g->parent = p;
  if(B!=NULL)
    B->parent = p;
  p->left = B;
  if(C!=NULL)
    C->parent = g;
  g->left = C;
  x->parent = ggp;
  if(ggp!=NULL)
    if(ggp->left ==g)
      ggp->left = x;
    else //??
      ggp->right =x;
}

void print_tree(SEARCH_TREE T){
  if(T!=NULL){
    print_tree(T->left);
    print_element(element);
    print_tree(T->right);
  }
}
